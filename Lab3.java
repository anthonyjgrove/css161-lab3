/**
 * Short one-line summary (e.g. "Lab 3, CSSSKL161")
 * 
 * 
 * @author Anthony Grove = CSSK - 161
 */
import java.util.Scanner;
public class Lab3 {
    public static void main(String[] args) {
        dayChecker();
    }

    public static void dayChecker(){
        Scanner keyboard = new Scanner(System.in);
        String month, month1;
        int day, year, c, y, L, m;

        System.out.println("Please enter a date in the following form: 1st three letters of a month (space) a day (space) a four digit year greater than 1600 (enter).");
        month1 = keyboard.next();
        day = keyboard.nextInt();
        year = keyboard.nextInt();

        month = month1.toLowerCase(); //standardize month input
        L = 2;//leap test value
        m = 00;//month # test value
        c = 99;//c # test value

        //calc if leap year, if l = 1 for leap year
        if (year % 100 == 0)
        {
            if (year % 400 == 0)
            {L = 1;}
            else
            {L = 0;}
        }
        if (year % 4 == 0)
        {L = 1;}
        else
        {L = 0;}

        //System.out.println(L); //test leap value
        //covert months to numbers
        if (month.equals("oct"))
        { m = 0;}
        if (month.equals("may"))
        { m = 1;}
        if (month.equals("mar") || month.equals("nov"))
        { m = 3;}
        if (month.equals("aug"))
        { m = 2;}
        if (month.equals("jun"))
        {  m = 4;}
        if (month.equals("apr") || month.equals("jul"))
        {  m = 6;}
        if (month.equals("sep") || month.equals("dec"))
        {  m = 5;}
        if (month.equals("jan"))
        {
            if (1 == L)
            {  m = 6;}
            else
            {  m = 0;}
        }
        if (month.equals("feb"))
        {
            if (1 == L)
            {  m = 2;}
            else
            {  m = 3;}
        }
        //System.out.println(m); //test m value
        y = year % 100;

        if  ((year / 100) % 4 == 0)
        {c = 6;}
        if  ((year / 100) % 4 == 1)
        {c = 4;}
        if  ((year / 100) % 4 == 2)
        {c = 2;}
        if  ((year / 100) % 4 == 3)
        {c = 0;}
        int magic = (day + m + y + y / 4 + c) % 7;
        String dow = "test";
        if (magic == 0)
        {dow = "Sunday!";}
        if (magic == 1)
        {dow = "Monday!";}
        if (magic == 2)
        {dow = "Tuesday!";}
        if (magic == 3)
        {dow = "Wednesday!";}
        if (magic == 4)
        {dow = "Thursday!";}
        if (magic == 5)
        {dow = "Friday!";}
        if (magic == 6)
        {dow = "Saturday!";}

        System.out.print(month1 + ", " + day + " " + year + " is a " + dow);

    }
}
